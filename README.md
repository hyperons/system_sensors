# RPI System sensors
[![GitHub Release][releases-shield]][releases]
[![License][license-shield]](LICENSE.md)

![Project Maintenance][maintenance-shield]
[![GitHub Activity][commits-shield]][commits]

[![Community Forum][forum-shield]][forum]

Add to crontab:
@reboot sleep 5 && cd /home/pi/system_sensors && /usr/bin/python3 src/system_sensors.py src/settings.yaml 1> /home/pi/system_settings.log 2> /home/pi/system_settings_err.log &

Simple script that runs on demand. It uses the MQTT Discovery for Home Assistant so you don’t need to configure anything in Home Assistant if you have discovery enabled for MQTT

It currently logs the following data:
* CPU usage
* CPU temperature
* Disk usage
* Memory usage
* Power status of the RPI
* Last boot
* Last message received timestamp
* Swap usage
* Wifi signal strength
* Amount of upgrades pending
* Disk usage of external drives
* Hostname
* Host local IP
* Host OS distro and version

# System Requirements

You need to have at least __python 3.6__ installed to use System Sensors.

# Installation:
1. Clone this repo >> git clone https://gitlab.com/hyperons/system_sensors
2. cd system_sensors
3. pip3 install -r requirements.txt
4. sudo apt-get install python3-apt
5. Edit settings_example.yaml in "~/system_sensors/src" to reflect your setup and save as settings.yaml:

| Value  | Required | Default | Description | 
| ------------- | ------------- | ------------- | ------------- |
| hostname  | true | \ | Hostname of the MQTT broker
| port  | false | 1883 | Port of the MQTT broker
| user | false | \ | The userlogin( if defined) for the MQTT broker
| password | false | \ | the password ( if defined) for the MQTT broker
| deviceName | true | \ | device name is sent with topic
| client_id | true | \ | client id to connect to the MQTT broker
| timezone | true | \ | Your local timezone (you can find the list of timezones here: [time zones](https://gist.github.com/heyalexej/8bf688fd67d7199be4a1682b3eec7568))
| power_integer_state(Deprecated) | false | false | Deprecated
| update_interval | false | 60 | The update interval to send new values to the MQTT broker 
| check_available_updates | false | false | Check the # of avaiblable updates 
| check_wifi_strength | false | false | Check the wifi strength 
| external_drives | false | \ | Declare external drives you want to check disk usage of (see example settings.yaml)

6. python3 src/system_sensors.py src/settings.yaml
7. (optional) Add script to crontab to run at regular interval:
    */5 * * * * cd /home/pi/system_sensors && /usr/bin/python3 src/system_sensors.py src/settings.yaml 1> /home/pi/system_settings.log 2> /home/pi/system_settings_err.log &

# Home Assistant configuration:
## Configuration:
The only config you need in Home Assistant is the following:
```yaml
mqtt:
  discovery: true
  discovery_prefix: homeassistant
```



[commits-shield]: https://img.shields.io/github/commit-activity/y/Sennevds/system_sensors?style=for-the-badge
[commits]: https://github.com/sennevds/system_sensors/commits/master
[forum-shield]: https://img.shields.io/badge/community-forum-brightgreen.svg?style=for-the-badge
[forum]: https://community.home-assistant.io/t/remote-rpi-system-monitor/129274
[license-shield]: https://img.shields.io/github/license/sennevds/system_sensors.svg?style=for-the-badge
[maintenance-shield]: https://img.shields.io/maintenance/yes/2020.svg?style=for-the-badge
[releases-shield]: https://img.shields.io/github/release/sennevds/system_sensors.svg?style=for-the-badge
[releases]: https://github.com/sennevds/system_sensors/releases
