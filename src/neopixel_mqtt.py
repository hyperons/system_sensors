#!/usr/bin/env python3
import argparse
import datetime as dt
import signal
import sys
import socket
import platform
import threading
import yaml
import time
from datetime import timedelta
from re import findall
from subprocess import check_output
from rpi_bad_power import new_under_voltage
import paho.mqtt.client as mqtt
import time
import board
import neopixel
import json


# Choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D18
# NeoPixels must be connected to D10, D12, D18 or D21 to work.
pixel_pin = board.D18

# The number of NeoPixels
num_pixels = 60

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
#ORDER = neopixel.GRB
ORDER = neopixel.GRBW

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.5, auto_write=False, pixel_order=ORDER
)


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b) if ORDER in (neopixel.RGB, neopixel.GRB) else (r, g, b, 0)



try:
    import apt
    apt_disabled = False
except ImportError:
    apt_disabled = True

mqttClient = None
WAIT_TIME_SECONDS = 60
deviceName = None
_underVoltage = None

class ProgramKilled(Exception):
    pass


def signal_handler(signum, frame):
    raise ProgramKilled


class Job(threading.Thread):
    def __init__(self, interval, execute, *args, **kwargs):
        threading.Thread.__init__(self)
        self.daemon = False
        self.stopped = threading.Event()
        self.interval = interval
        self.execute = execute
        self.args = args
        self.kwargs = kwargs

    def stop(self):
        self.stopped.set()
        self.join()

    def run(self):
        while not self.stopped.wait(self.interval.total_seconds()):
            self.execute(*self.args, **self.kwargs)


def write_message_to_console(message):
    print(message)
    sys.stdout.flush()
    
def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(pixel_index & 255)
        pixels.show()
        time.sleep(wait)

def fill(color):
    pixels.fill(color)
    pixels.show()

def bar(percentage, fgcolor, bgcolor):
    top = int(int(percentage)/100*num_pixels)
    for i in range(num_pixels):
        if i < top:
            pixels[i] = bgcolor
        else:
            pixels[i] = fgcolor
    pixels.show()

def bar_animated(percentage, fgcolor, bgcolor, delay):
    fill(fgcolor);
    top = int(int(percentage)/100*num_pixels)
    for i in range(num_pixels):
        if i < top:
            pixels[i] = bgcolor
        else:
            pixels[i] = fgcolor
        time.sleep(delay)
        pixels.show()


def on_message(client, userdata, message):
    print (f"Message received: {message.payload.decode()} {dt.datetime.now()}"  )
    mess = message.payload.decode()
    cmd_json = json.loads(mess)
    cmd = cmd_json["pattern"]

    # RAINBOW
    if cmd == "rainbow":
       delay=float(cmd_json["delay"])
       for i in range(cmd_json["repeat"]):
          rainbow_cycle(delay)
    # FILL
    elif cmd == "fill":
       fill(tuple(map(int, cmd_json["bgcolor"].split(','))))

    # BAR
    elif cmd == "bar":
       fill((0,0,0))
       bar(cmd_json["percentage"], tuple(map(int, cmd_json["bgcolor"].split(','))), tuple(map(int, cmd_json["fgcolor"].split(','))))

    # ANIMATED BAR
    elif cmd == "bar_animated":
       fill((0,0,0))
       delay=float(cmd_json["delay"])
       for i in range(cmd_json["repeat"]):
          bar_animated(cmd_json["percentage"], tuple(map(int, cmd_json["bgcolor"].split(','))), tuple(map(int, cmd_json["fgcolor"].split(','))), delay)

    # FLASH
    elif cmd == "flash":
       fill((0,0,0))
       delay=float(cmd_json["delay"])
       for i in range(cmd_json["repeat"]):
          fill(tuple(map(int, cmd_json["bgcolor"].split(','))))
          time.sleep(delay)
          fill(tuple(map(int, cmd_json["fgcolor"].split(','))))
          time.sleep(delay)
   

def get_updates():
    cache = apt.Cache()
    cache.open(None)
    cache.upgrade()
    return str(cache.get_changes().__len__())

def send_config_message(mqttClient):
    write_message_to_console("Send config message")


def _parser():
    """Generate argument parser"""
    parser = argparse.ArgumentParser()
    parser.add_argument("settings", help="path to the settings file")
    return parser


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        write_message_to_console(f"Connected to broker {deviceName}")
        client.subscribe(f"neopixel/{deviceName}/pattern")
    else:
        write_message_to_console("Connection failed")


if __name__ == "__main__":
    args = _parser().parse_args()
    with open(args.settings) as f:
        # use safe_load instead load
        settings = yaml.safe_load(f)
    if "update_interval" in settings:
        WAIT_TIME_SECONDS = settings["update_interval"]
    mqttClient = mqtt.Client(client_id=settings["client_id"])
    mqttClient.on_connect = on_connect                      #attach function to callback
    mqttClient.on_message = on_message
    deviceName = settings["deviceName"].replace(" ", "").lower()
    deviceNameDisplay = settings["deviceName"]
    if "user" in settings["mqtt"]:
        mqttClient.username_pw_set(
            settings["mqtt"]["user"], settings["mqtt"]["password"]
        )  # Username and pass if configured otherwise you should comment out this
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)
    if "port" in settings["mqtt"]:
        write_message_to_console("Using custom port")
        mqttClient.connect(settings["mqtt"]["hostname"], settings["mqtt"]["port"])
    else:
        mqttClient.connect(settings["mqtt"]["hostname"], 1883)

    rainbow_cycle(0.1)
    mqttClient.loop_forever()
